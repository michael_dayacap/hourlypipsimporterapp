/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hourlypipsimporter;

import com.granberg.nadexparserimporter.controller.ControllerFactory;
import com.granberg.nadexparserimporter.importer.HourlyPipsImporter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mdayacap
 */
public class Main {

    private static final String APP_PROP_PATH = "./app.properties";
    private static final String HOURLYPIPS_INSTRUMENT_NAME = "hourlypips.instrumentname";
    private static final String HOURLYPIPS_END_DATE = "hourlypips.enddate";
    private static final String MOVING_AVERAGE_IN_DAYS = "hourlypips.movingaverageindays";
    private static final String HOURS = "hourlypips.hours";
    private static final Properties app;

    static {
        app = new Properties();
        try {
            app.load(new FileInputStream(APP_PROP_PATH));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void main(String[] args) throws ParseException {
        importHourlyPips();
    }

    public static void importHourlyPips() throws ParseException {
        ControllerFactory cf = ControllerFactory.getControllerFactory(ControllerFactory.MYSQL);
        HourlyPipsImporter importer = new HourlyPipsImporter(cf.getForexJpaController(), cf.getHourlyPipsJpaController());
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String instrumentName = app.getProperty(HOURLYPIPS_INSTRUMENT_NAME);
        Date endDate = df.parse(app.getProperty(HOURLYPIPS_END_DATE));
        int hours = Integer.parseInt(app.getProperty(HOURS));
        int movingAverageInDays = Integer.parseInt(app.getProperty(MOVING_AVERAGE_IN_DAYS));

        importer.importHourlyPips(instrumentName, endDate, hours, movingAverageInDays);
    }
}
